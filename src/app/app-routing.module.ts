import {NgModule, Component} from '@angular/core';
import {Routes, RouterModule, CanActivate} from '@angular/router';
import {DoctorsListComponent} from './pages/doctor/doctor-list/doctors-list.component';
import {AuthGuard} from './security/auth.guard';
import {LoginComponent} from './pages/authorization/login/login.component';
import {HomeComponent} from './pages/home/home.component';
import {RegisterComponent} from './pages/authorization/register/register.component';
import {ConfirmationVisitComponent} from './pages/patient/confirmation-visit/confirmation-visit.component';
import {AuthGuardPatient} from './security/auth.guard.patient';
import {PatientVisitListComponent} from './pages/patient/patient-visit-list/patient-visit-list.component';
import {ConfirmationMessageComponent} from './pages/patient/confirmation-message/confirmation-message.component';
import {DoctorVisitListComponent} from './pages/doctor/doctor-visit-list/doctor-visit-list.component';
import {AuthGuardDoctor} from './security/auth.guard.doctor';
import {DoctorAccountComponent} from './pages/doctor/doctor-account/doctor-account.component';
import {PatientAccountComponent} from './pages/patient/patient-account/patient-account.component';
import {AuthGuardIsLogin} from './security/auth.guard.is.login';

export const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [AuthGuardIsLogin]
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [AuthGuardIsLogin]
  },
  {
    path: 'doctor-list',
    component: DoctorsListComponent,
  },
  {
    path: 'doctor/visit-list',
    component: DoctorVisitListComponent,
    canActivate: [AuthGuardDoctor]
  },
  {
    path: 'doctor/account',
    component: DoctorAccountComponent,
    canActivate: [AuthGuardDoctor]
  },
  {
    path: 'patient/visit-list',
    component: PatientVisitListComponent,
    canActivate: [AuthGuardPatient]
  },
  {
    path: 'patient/account',
    component: PatientAccountComponent,
    canActivate: [AuthGuardPatient]
  },
  {
    path: 'make-appointment/doctor/:idDoctor/visit/:idVisit',
    component: ConfirmationVisitComponent,
    canActivate: [AuthGuardPatient]
  },
  {
    path: 'make-appointment/confirmation',
    component: ConfirmationMessageComponent,
    canActivate: [AuthGuardPatient]
  },
  {path: '', component: HomeComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
