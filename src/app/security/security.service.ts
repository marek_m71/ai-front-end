import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {
  auth = {id: 0, fullName: '', sub: '', role: [], exp: 0};

  constructor(public jwtHelper: JwtHelperService) {
  }

  public isAuthenticated(): boolean {
    return this.isAuthenticatedPatient() || this.isAuthenticatedDoctor();
  }

  public isAuthenticatedPatient(): boolean {
    const token = localStorage.getItem('Authorization');
    if (this.jwtHelper.decodeToken(token) !== null) {
      this.auth = this.jwtHelper.decodeToken(token);
      return (
        this.auth.role.includes('ROLE_PATIENT') && !this.jwtHelper.isTokenExpired(token)
      );
    }
    return false;
  }

  public isAuthenticatedDoctor(): boolean {
    const token = localStorage.getItem('Authorization');
    if (this.jwtHelper.decodeToken(token) !== null) {
      this.auth = this.jwtHelper.decodeToken(token);
      return (
        this.auth.role.includes('ROLE_DOCTOR') && !this.jwtHelper.isTokenExpired(token)
      );
    }
    return false;
  }

  resetSecurityObject(): void {
    localStorage.removeItem('Authorization');
  }

  public getFullName(): string {
    return this.auth.fullName;
  }

  public getId(): number {
    return this.auth.id;
  }
}
