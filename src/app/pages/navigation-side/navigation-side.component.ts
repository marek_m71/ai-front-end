import { Component, OnInit } from '@angular/core';
import {SecurityService} from '../../security/security.service';

@Component({
  selector: 'app-navigation-side',
  templateUrl: './navigation-side.component.html',
  styleUrls: ['./navigation-side.component.css']
})
export class NavigationSideComponent implements OnInit {

  constructor(public securityService: SecurityService) { }

  ngOnInit() {
  }

  logout(): void {
    this.securityService.resetSecurityObject();
  }
}
