import {Component, OnInit} from '@angular/core';
import {SpecializationDto} from '../../model/specialization-dto';
import {DoctorService} from '../../services/doctor-services/doctor.service';
import {SearchDoctorsForm} from '../../model/form/search-doctors-form';
import {DoctorDto} from '../../model/doctor-dto';
import {MatSnackBar, MatTableDataSource} from '@angular/material';
import {Router} from '@angular/router';
import {SpecializationService} from '../../services/specialization-service/specialization.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  specializationList: SpecializationDto[] = [];
  searchDoctor: SearchDoctorsForm = new SearchDoctorsForm();

  constructor(private doctorService: DoctorService,
              private router: Router,
              private snackBar: MatSnackBar,
              private specializationService: SpecializationService) {
  }

  ngOnInit() {
    this.getSpecializations();
  }

  searchDoctors() {
    console.log(this.searchDoctor);
    this.doctorService.findSome(this.searchDoctor).subscribe(
      resp => {
        this.doctorService.doctorList = resp;
        this.router.navigate(['/doctor-list']);
      },
      error => {
        this.openSnackBar('Ups, wystąpił problem z serwerem, spróbuj ponownie', 'Zamknij');
      }
    );
  }

  getSpecializations() {
    this.specializationService.findAll().subscribe(
      resp => {
        this.specializationList = resp;
      },
      error => {
        this.openSnackBar('Ups, wystąpił problem z serwerem, spróbuj ponownie', 'Zamknij');
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 4000,
    });
  }
}
