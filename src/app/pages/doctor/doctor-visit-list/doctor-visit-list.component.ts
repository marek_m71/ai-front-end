import {Component, OnInit} from '@angular/core';
import {DoctorService} from '../../../services/doctor-services/doctor.service';
import {DoctorDetailsDto} from '../../../model/doctor-details-dto';
import {MatSnackBar} from '@angular/material';
import {SecurityService} from '../../../security/security.service';
import {PatientService} from '../../../services/patient-services/patient.service';
import {VisitService} from '../../../services/visit-services/visit.service';
import {OperationVisitForm} from '../../../model/form/operation-visit-form';

@Component({
  selector: 'app-doctor-visit-list',
  templateUrl: './doctor-visit-list.component.html',
  styleUrls: ['./doctor-visit-list.component.css']
})
export class DoctorVisitListComponent implements OnInit {

  idDoctor: number;
  doctorDto: DoctorDetailsDto;
  filter = {date: new Date()};
  nowDate = new Date();

  constructor(private doctorService: DoctorService,
              private snackBar: MatSnackBar,
              private securityService: SecurityService,
              private visitService: VisitService) {
  }

  ngOnInit() {
    this.idDoctor = this.securityService.getId();
    this.findDoctorDetails(this.idDoctor);
  }

  findDoctorDetails(id: number) {
    this.doctorService.findDetails(id).subscribe(
      resp => {
        this.doctorDto = resp;
        this.doctorDto.visitList.forEach(r => {
          const date = new Date(r.dateTimeVisit);
          const userTimezoneOffset = date.getTimezoneOffset() * 60000;
          r.dateTimeVisit = new Date(date.getTime() - userTimezoneOffset);
        });
      },
      error => {
        this.openSnackBar('Ups, wystąpił problem z serwerem, spróbuj ponownie', 'Zamknij');
      }
    );
  }

  cancelDoctorVisit(id: number) {
    const operationVisitForm = new OperationVisitForm(id);
    this.visitService.cancelDoctorVisit(operationVisitForm).subscribe(
      resp => {
        this.findDoctorDetails(this.idDoctor);
      },
      error => {
        this.openSnackBar('Ups, wystąpił problem z serwerem, spróbuj ponownie', 'Zamknij');
      }
    );
  }

  deleteDoctorVisit(id: number) {
    this.visitService.deleteDoctorVisit(id).subscribe(
      resp => {
        this.findDoctorDetails(this.idDoctor);
      },
      error => {
        this.openSnackBar('Ups, wystąpił problem z serwerem, spróbuj ponownie', 'Zamknij');
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 4000,
    });
  }
}
