import {Component, OnInit} from '@angular/core';
import {LoginService} from '../../../services/login-services/login-service/login.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user = {login: '', password: ''};
  hide = true;
  error = false;
  errorDescription = '';
  return = '';

  constructor(private connection: LoginService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => this.return = params['return'] || '/home');
  }

  loginConnect() {
    this.connection.login(this.user.login, this.user.password).subscribe(
      resp => {
        localStorage.setItem('Authorization', resp['Authorization']);
        this.router.navigateByUrl(this.return);
      },
      error => {
        if (error['status'] === 403) {
          this.error = true;
          this.errorDescription = 'Niepoprawny login lub hasło.';
        } else {
          this.error = true;
          this.errorDescription = 'Nieznany błąd podczas logowania (serwer nieosiągalny).';
        }
      }
    );
  }
}
