import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmationVisitComponent } from './confirmation-visit.component';

describe('ConfirmationVisitComponent', () => {
  let component: ConfirmationVisitComponent;
  let fixture: ComponentFixture<ConfirmationVisitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmationVisitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmationVisitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
