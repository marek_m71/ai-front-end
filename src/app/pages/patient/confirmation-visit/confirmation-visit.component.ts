import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DoctorDto} from '../../../model/doctor-dto';
import {VisitDto} from '../../../model/visit-dto';
import {VisitService} from '../../../services/visit-services/visit.service';
import {DoctorService} from '../../../services/doctor-services/doctor.service';
import {PatientService} from '../../../services/patient-services/patient.service';
import {PatientDto} from '../../../model/patient-dto';
import {SecurityService} from '../../../security/security.service';
import {MatSnackBar} from '@angular/material';
import {MakeAppointmentForm} from '../../../model/form/make-appointment-form';

@Component({
  selector: 'app-confirmation-visit',
  templateUrl: './confirmation-visit.component.html',
  styleUrls: ['./confirmation-visit.component.css']
})
export class ConfirmationVisitComponent implements OnInit {

  idDoctor: number;
  idVisit: number;
  doctorDto: DoctorDto;
  patientDto: PatientDto;
  visitDto: VisitDto;
  idPatient: number;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private visitService: VisitService,
              private doctorService: DoctorService,
              private patientService: PatientService,
              private securityService: SecurityService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.idPatient = this.securityService.getId();
    this.route.params.subscribe(params => {
      this.idDoctor = +params['idDoctor'];
      this.idVisit = +params['idVisit'];
    });
    this.findDoctor(this.idDoctor);
    this.findVisit(this.idVisit);
    this.findPatient(this.idPatient);
  }

  findDoctor(id: number) {
    this.doctorService.find(id).subscribe(
      resp => {
        this.doctorDto = resp;
      },
      error => {
        this.openSnackBar('Ups, wystąpił problem z serwerem, spróbuj ponownie', 'Zamknij');
      }
    );
  }

  findPatient(id: number) {
    this.patientService.find(id).subscribe(
      resp => {
        this.patientDto = resp;
      },
      error => {
        this.openSnackBar('Ups, wystąpił problem z serwerem, spróbuj ponownie', 'Zamknij');
      }
    );
  }

  findVisit(id: number) {
    this.visitService.find(id).subscribe(
      resp => {
        this.visitDto = resp;
        const date = new Date(this.visitDto.dateTimeVisit);
        const userTimezoneOffset = date.getTimezoneOffset() * 60000;
        this.visitDto.dateTimeVisit = new Date(date.getTime() - userTimezoneOffset);
      },
      error => {
        this.openSnackBar('Ups, wystąpił problem z serwerem, spróbuj ponownie', 'Zamknij');
      }
    );
  }

  confirm() {
    const makeAppointmentForm = new MakeAppointmentForm(this.patientDto.id, this.visitDto.id);
    this.patientService.makeAppointment(makeAppointmentForm).subscribe(
      resp => {
        this.router.navigate(['make-appointment/confirmation']);
      },
      error => {
        this.openSnackBar('Ups, coś poszło nie tak, spróbuj ponownie', 'Zamknij');
        this.router.navigate(['home']);
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 4000,
    });
  }
}
