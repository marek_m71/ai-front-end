import {Component, OnInit} from '@angular/core';
import {SecurityService} from '../../../security/security.service';
import {PatientService} from '../../../services/patient-services/patient.service';
import {MatSnackBar} from '@angular/material';
import {PatientDto} from '../../../model/patient-dto';
import {PasswordService} from '../../../services/login-services/password-service/password.service';
import {Router} from '@angular/router';
import {ChangePasswordForm} from '../../../model/form/change-password-form';

@Component({
  selector: 'app-patient-account',
  templateUrl: './patient-account.component.html',
  styleUrls: ['./patient-account.component.css']
})
export class PatientAccountComponent implements OnInit {
  password = {oldPassword: '', password: '', confirmPassword: ''};
  hide = true;
  hide1 = true;
  hide2 = true;
  idPatient: number;
  patientDto: PatientDto;

  constructor(private securityService: SecurityService,
              private patientService: PatientService,
              private snackBar: MatSnackBar,
              private passwordService: PasswordService,
              private router: Router) {
  }

  ngOnInit() {
    this.idPatient = this.securityService.getId();
    this.findPatient(this.idPatient);
  }

  findPatient(id: number) {
    this.patientService.find(id).subscribe(
      resp => {
        this.patientDto = resp;
        this.patientDto.visitList.forEach(r => {
          const date = new Date(r.dateTimeVisit);
          const userTimezoneOffset = date.getTimezoneOffset() * 60000;
          r.dateTimeVisit = new Date(date.getTime() - userTimezoneOffset);
        });
      },
      error => {
        this.openSnackBar('Ups, wystąpił problem z serwerem, spróbuj ponownie', 'Zamknij');
      }
    );
  }

  updatePatient() {
    this.patientService.update(this.patientDto.id, this.patientDto).subscribe(
      resp => {
        this.patientDto = resp;
      },
      error => {
        this.openSnackBar('Ups, wystąpił problem z serwerem, spróbuj ponownie', 'Zamknij');
      }
    );
  }

  changePassword() {
    if (this.matchPassword()) {
      const changePasswordForm = new ChangePasswordForm(this.idPatient, this.password.oldPassword, this.password.password);
      this.passwordService.changePassword(changePasswordForm).subscribe(
        resp => {
          this.router.navigate(['home']);
          this.openSnackBar('Hasło zostało zmienione', 'Zamknij');
        },
        error => {
          if (error['status'] === 409) {
            this.openSnackBar('Stare hasło jest niepoprawne', 'Zamknij');
          } else {
            this.openSnackBar('Ups, wystąpił problem z serwerem, spróbuj ponownie', 'Zamknij');
          }
        }
      );
    } else {
      this.openSnackBar('Podane hasła są różne', 'Zamknij');
    }
  }

  matchPassword(): boolean {
    return this.password.password === this.password.confirmPassword;
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 4000,
    });
  }
}
