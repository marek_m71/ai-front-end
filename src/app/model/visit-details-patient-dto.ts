import {PatientInformationDto} from './patient-information-dto';

export class VisitDetailsPatientDto {
  id: number;
  dateTimeVisit: Date;
  isAppointed: boolean;
  isCanceledByDoctor: boolean;
  patient: PatientInformationDto;
}
