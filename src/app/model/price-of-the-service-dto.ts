export class PriceOfTheServiceDto {
  id: number;
  name: string;
  price: number;
}
