import {SpecializationDto} from './specialization-dto';
import {PriceOfTheServiceDto} from './price-of-the-service-dto';
import {VisitDto} from './visit-dto';
import {OpinionDto} from './opinion-dto';

export class DoctorDto {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  city: string;
  specializationList: SpecializationDto[];
  priceList: PriceOfTheServiceDto[];
  visitList: VisitDto[];
  opinionList: OpinionDto[];
}
