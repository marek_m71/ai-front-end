export class VisitDto {
  id: number;
  dateTimeVisit: Date;
  isAppointed: boolean;
  isCanceledByDoctor: boolean;

}
