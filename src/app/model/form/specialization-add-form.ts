import {SpecializationDto} from '../specialization-dto';

export class SpecializationAddForm {
  idDoctor: number;
  specialization: SpecializationDto;
}
