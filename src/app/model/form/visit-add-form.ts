import {VisitForm} from './visit-form';

export class VisitAddForm {
  idDoctor: number;
  visitList: VisitForm[] = [];
}
