import {SpecializationDto} from '../specialization-dto';
import {PriceOfTheServiceDto} from '../price-of-the-service-dto';
import {VisitForm} from './visit-form';
import {PriceOfTheServiceForm} from './price-of-the-service-form';

export class DoctorRegisterForm {
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  city: string;
  specializationList: SpecializationDto[] = [];
  priceList: PriceOfTheServiceForm[] = [];
  visitList: VisitForm[] = [];
}
