export class MakeAppointmentForm {
  idPatient: number;
  idVisit: number;

  constructor(idPatient: number, idVisit: number) {
    this.idPatient = idPatient;
    this.idVisit = idVisit;
  }
}
