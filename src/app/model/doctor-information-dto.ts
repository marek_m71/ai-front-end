import {SpecializationDto} from './specialization-dto';
import {PriceOfTheServiceDto} from './price-of-the-service-dto';
import {VisitDto} from './visit-dto';

export class DoctorInformationDto {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  city: string;
  specializationList: SpecializationDto[];
  priceList: PriceOfTheServiceDto[];
}
