import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavigationSideComponent } from './pages/navigation-side/navigation-side.component';
import {MaterialModule} from './modules/material/material.module';
import {RouterModule} from '@angular/router';
import { DoctorsListComponent } from './pages/doctor/doctor-list/doctors-list.component';
import { LoginComponent } from './pages/authorization/login/login.component';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {JsonTypeInterceptor} from './interceptors/json-type-interceptor.service';
import {FormsModule} from '@angular/forms';
import {JwtHelperService, JwtModule} from '@auth0/angular-jwt';
import {DoctorService} from './services/doctor-services/doctor.service';
import {AuthGuard} from './security/auth.guard';
import {LoginService} from './services/login-services/login-service/login.service';
import {AppRoutingModule} from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './pages/home/home.component';
import { RegisterComponent } from './pages/authorization/register/register.component';
import { ConfirmationVisitComponent } from './pages/patient/confirmation-visit/confirmation-visit.component';
import {AuthGuardPatient} from './security/auth.guard.patient';
import {AuthGuardDoctor} from './security/auth.guard.doctor';
import {PatientVisitListComponent} from './pages/patient/patient-visit-list/patient-visit-list.component';
import { ConfirmationMessageComponent } from './pages/patient/confirmation-message/confirmation-message.component';
import { DoctorVisitListComponent } from './pages/doctor/doctor-visit-list/doctor-visit-list.component';
import { DoctorAccountComponent } from './pages/doctor/doctor-account/doctor-account.component';
import { PatientAccountComponent } from './pages/patient/patient-account/patient-account.component';
import {AuthGuardIsLogin} from './security/auth.guard.is.login';
import { OpinionAddDialogComponent } from './pages/opinion/opinion-add-dialog/opinion-add-dialog.component';
import { OpinionListDialogComponent } from './pages/opinion/opinion-list-dialog/opinion-list-dialog.component';

export function tokenGetter() {
  return localStorage.getItem('Authorization');
}

@NgModule({
  declarations: [
    AppComponent,
    NavigationSideComponent,
    DoctorsListComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
    ConfirmationVisitComponent,
    PatientVisitListComponent,
    ConfirmationMessageComponent,
    DoctorVisitListComponent,
    DoctorAccountComponent,
    PatientAccountComponent,
    OpinionAddDialogComponent,
    OpinionListDialogComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    AppRoutingModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ['localhost:8080'],
        skipWhenExpired: true,
        headerName: 'Authorization',
        authScheme: ''
      }
    }),
    BrowserAnimationsModule
  ],
  providers: [
    DoctorService,
    JwtHelperService,
    AuthGuard,
    AuthGuardPatient,
    AuthGuardDoctor,
    AuthGuardIsLogin,
    HttpClient,
    LoginService,
    { provide: HTTP_INTERCEPTORS, useClass: JsonTypeInterceptor, multi: true}
  ],
  entryComponents: [
    OpinionAddDialogComponent,
    OpinionListDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
