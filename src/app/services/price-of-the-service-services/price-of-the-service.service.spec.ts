import { TestBed } from '@angular/core/testing';

import { PriceOfTheServiceService } from './price-of-the-service.service';

describe('PriceOfTheServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PriceOfTheServiceService = TestBed.get(PriceOfTheServiceService);
    expect(service).toBeTruthy();
  });
});
