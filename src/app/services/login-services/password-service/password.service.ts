import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {PatientRegisterForm} from '../../../model/form/patient-register-form';
import {Observable} from 'rxjs';
import {ChangePasswordForm} from '../../../model/form/change-password-form';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class PasswordService {

  constructor(private httpClient: HttpClient) {
  }

  changePassword(changePasswordForm: ChangePasswordForm): Observable<any> {
    return this.httpClient.post(apiUrl + environment.password.change, JSON.stringify(changePasswordForm));
  }
}
