import { Injectable } from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private httpClient: HttpClient) { }

  login(loginUsername: string, loginPassword: string): Observable<any> {
    return this.httpClient.post(
      apiUrl + environment.login,
      JSON.stringify({ username: loginUsername, password: loginPassword })
    );
  }
}
