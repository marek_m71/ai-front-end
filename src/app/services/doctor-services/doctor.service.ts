import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {SpecializationDto} from '../../model/specialization-dto';
import {SearchDoctorsForm} from '../../model/form/search-doctors-form';
import {DoctorDto} from '../../model/doctor-dto';
import {MedicalServiceDto} from '../../model/medical-service-dto';
import {DoctorDetailsDto} from '../../model/doctor-details-dto';
import {PatientDto} from '../../model/patient-dto';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class DoctorService {

  doctorList: DoctorDto[] = [];

  constructor(private httpClient: HttpClient) {
  }

  find(id: number): Observable<DoctorDto> {
    return this.httpClient.get<DoctorDto>(apiUrl + environment.doctor.doctors + id);
  }

  findDetails(id: number): Observable<DoctorDetailsDto> {
    return this.httpClient.get<DoctorDetailsDto>(apiUrl + environment.doctor.doctors + id + environment.doctor.details);
  }

  findSome(searchDoctor: SearchDoctorsForm): Observable<DoctorDto[]> {
    return this.httpClient.post<DoctorDto[]>(apiUrl + environment.doctor.findSome, JSON.stringify(searchDoctor));
  }

  update(id: number, doctor: DoctorDto): Observable<DoctorDto> {
    return this.httpClient.put<DoctorDto>(apiUrl + environment.doctor.doctors + id, JSON.stringify(doctor));
  }
}
